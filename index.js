/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/
		//1.A --> ADDITION <--

		function displaySum(add1, add2){
			let sum = add1 + add2; 
			console.log("Displayed sum of 5 and 15 ");
			console.log(sum);
		};

		displaySum(5, 15);

		//1.B --> SUBTRACTION <---

		function displayDifference(sub1, sub2){
			let difference = sub1 - sub2; 
			console.log("Displayed difference of 20 and 5 ");
			console.log(difference);
		};

		displayDifference(20, 5);

/*	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication. (use return statement)

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division. (use return statement)

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.*/

		// 2.A --> SOLUTION <--

		function returnProduct(mult1, mult2){

			return mult1 * mult2
		};

		let product = returnProduct(50 , 10);
		console.log("Displayed product of 50 and 10: ");
		console.log(product);

		// 2.B --> SOLUTION <--

		function returnQuotient(div1, div2){

			return div1 / div2
		};

		let quotient = returnQuotient(50 , 10);
		console.log("Displayed quotient of 50 and 10: ");
		console.log(quotient);


/*	3. 	Create a function which will be able to get total area of a circle from a provided radius.
			-a number should be provided as an argument.
			-formula: area = pi * (radius ** 2)
			-exponent operator **
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation. (use return statement)

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

		Log the value of the circleArea variable in the console.*/

		// 3 --> SOLUTION <--

		function returnArea(pi, radius, exponent){

			return pi * (radius ** exponent)
		};

		let circleArea = returnArea(3.1416 , 5 , 2);
		console.log("The result of getting the area of a circle with a 5 radius: ");
		console.log(circleArea);

/*	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-formula for average: num1 + num2 + num3 + ... divide by total numbers
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation. (use return statement) 

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.*/

			// 4 --> SOLUTION <--

			function returnAverage(num1, num2, num3, num4){

				return (num1 + num2 + num3 + num4) / 4
			};

			let averageVar = returnAverage(5, 10, 15, 20);
			console.log("The average of 5, 10, 15, and 20: ");
			console.log(averageVar);

/*	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed. (use return statement)
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

			// 5 --> SOLUTION <--

			function returnPercentage(myScore, totalScore){

				return (myScore / totalScore) * 100
			};

			let isPassingScore = returnPercentage(95, 100);
			console.log("Is " + isPassingScore + " a passing score?")
			let isPassed = isPassingScore > 75
			console.log(isPassed);
